# Guess the subreddit 

Posts from the reddit frontpage are randomly selected including their description/picture. You have to guess which subreddit the post belongs to.

## TODO

* need to load more posts from the frontpage and/or load posts which were on the frontpage
* find a way to load json data from reddit without tripping cors
* UI improvements
* start the game when json data is finished loading (currently it is not waiting)
* highscore (?)
* show suggestions based on which subreddits were loaded
* improve picture sizing (add max_height/max_width)
